let formKw;
let formKwGas;
let formPlz;

const inputElement = document.getElementById("form_kw");

formKw = document.getElementById("form_kw");
formKwGas = document.getElementById("form_kwgas");
formPlz = document.getElementById("form_plz");

formKw.disabled = true;
formKwGas.disabled = true;
formPlz.disabled = true;

// navigation
function navigation() {
	const navLinks = document.getElementById("navLinks");

	navLinks.classList.toggle("active");
}

// Slider

let slideIndex = 0;

function showSlides() {
	let slides = document.querySelectorAll(".Card");
	let totalSlides = slides.length;

	for (let i = 0; i < totalSlides; i++) {
		slides[i].style.transform = `translateX(-${slideIndex * 100}%)`;
	}

	slideIndex++;
	if (slideIndex >= totalSlides) {
		slideIndex = 0;
	}

	setTimeout(showSlides, 6000);
}
showSlides();

// Accordeon

function accordeon(card) {
	let content = card.nextElementSibling;
	var accHeaders = document.querySelectorAll(".accordion-header");
	if (content.style.maxHeight) {
		content.style.maxHeight = null;
	} else {
		accHeaders.forEach(function (otherHeader) {
			if (otherHeader !== header) {
				otherHeader.nextElementSibling.style.maxHeight = null;
			}
		});
		content.style.maxHeight = content.scrollHeight + "px";
	}
}

let stromSelected = false;
let gasSelected = false;

function boxStrom() {
	stromSelected = !stromSelected;

	if (stromSelected === true) {
		document.getElementById("box-strom").classList.remove("box-a-option");
		document.getElementById("box-strom").classList.add("box-a-option-selected");
		document.getElementById("form_kw").disabled = false;
		document.getElementById("form_plz").disabled = false;
		document.getElementById("calculate-submit").disabled = false;
	} else {
		document
			.getElementById("box-strom")
			.classList.remove("box-a-option-selected");
		document.getElementById("box-strom").classList.add("box-a-option");
		document.getElementById("form_kw").disabled = true;
		document.getElementById("form_kw").value = "";
		if (gasSelected === false) {
			document.getElementById("form_plz").disabled = true;
			document.getElementById("form_plz").value = "";
			document.getElementById("calculate-submit").disabled = true;
		}
	}
}

function boxGas() {
	gasSelected = !gasSelected;

	if (gasSelected === true) {
		document.getElementById("box-gas").classList.remove("box-a-option");
		document.getElementById("box-gas").classList.add("box-a-option-selected");
		document.getElementById("form_kwgas").disabled = false;
		document.getElementById("form_plz").disabled = false;
		document.getElementById("calculate-submit").disabled = false;
	} else {
		document
			.getElementById("box-gas")
			.classList.remove("box-a-option-selected");
		document.getElementById("box-gas").classList.add("box-a-option");
		document.getElementById("form_kwgas").disabled = true;
		document.getElementById("form_kwgas").value = "";
		if (stromSelected === false) {
			document.getElementById("form_plz").disabled = true;
			document.getElementById("form_plz").value = "";
			document.getElementById("calculate-submit").disabled = true;
		}
	}
}

// get co2bilanz
var co2bilanzUrl = "https://dev.wirgemeinsam.wgkd.de/co2bilanz";

function getCo2bilanz() {
	fetch(co2bilanzUrl)
		.then(response => response.json())
		.then(data => {
			// change city value with response data
			console.log(data.co2bilanz);
			if (data.co2bilanz != null) {
				console.log(data.co2bilanz);
				document.querySelectorAll(".co2bilanz").forEach(element => {
					console.log(element);
					element.innerHTML = data.co2bilanz
						.toString()
						.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
				});
			}
		});
}

getCo2bilanz();
