---
title: "first page"
---

„Wir gemeinsam“ ist ein Energieangebot für Mitarbeitende in Kirchen und
sozialen Einrichtungen und deren Angehörige. Um Ihnen zertifizierten
Ökostrom sowie CO2-kompensiertes Erdgas anbieten zu können, sind die
Wirtschaftsgesellschaft der Kirchen in Deutschland (WGKD) und die WSW
Energie & Wasser AG eine Vertriebskooperation eingegangen. Dadurch kommen
Sie in den Genuss attraktiver Energiepreise und wir können den CO2-Ausstoß
mit dem KlimaINVEST-Zertifikat ausgleichen."

[Go to home](/)
